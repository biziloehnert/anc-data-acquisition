#!/usr/bin/env python3

import gitlab
import argparse
import sys

parser = argparse.ArgumentParser(description='Create GitLab MR for a new branch.')
parser.add_argument(
    'project_id',
    help = 'GitLab project id for which the MR wll be created'
)
parser.add_argument(
    'mr_iid',
    help = 'MR iid to which submit a note'
)
parser.add_argument(
    'note_body',
    help = 'MR note body'
)
args = parser.parse_args()

gl = gitlab.Gitlab.from_config('anc-data-acquisition')

project = gl.projects.get(args.project_id)

# Get MR
try:
    mr = project.mergerequests.get(args.mr_iid)
except Exception as e:
    print('[ERROR]', e)
    sys.exit(1)

# Create MR note
try:
    mr.notes.create({
        'body': args.note_body
    })
except Exception as e:
    print('[ERROR]', e)
    sys.exit(1)
