#!/usr/bin/env python3

import gitlab
import argparse
import sys
import base64
from pathlib import Path
import os
from collections import defaultdict

parser = argparse.ArgumentParser(description = 'Add directory and all files in it to GitLab MR branch. Preserve subdirectories.')
parser.add_argument(
    'project_id',
    help = 'GitLab project id for which the MR will be created'
)
parser.add_argument(
    'branch_name',
    help = 'MR branch to which commit the file'
)
parser.add_argument(
    'dir_path',
    help = 'Path to directory to commit'
)
parser.add_argument(
    'repository_path',
    help = 'Path in the repository under which all files are committed to'
)
parser.add_argument(
    'commit_message',
    help = 'Commit message'
)
parser.add_argument(
    '-b', '--binary',
    dest = 'binary_ext',
    nargs = '+',
    help = 'List of file extensions (final components with a dot) that should be committed as binary',
)

args = parser.parse_args()

gl = gitlab.Gitlab.from_config('anc-data-acquisition')

project = gl.projects.get(args.project_id)

dir_path = Path(args.dir_path)
repository_path = Path(args.repository_path)


try:
    # Group files by basename
    files_by_basename = defaultdict(list)
    for path in dir_path.rglob('*.*'):
        base_name = path.stem
        files_by_basename[base_name].append(path)

    # Iterate over groups
    for index, (base_name, paths) in enumerate(files_by_basename.items()):
        actions = []
        #Iterate over files in groups
        for path in paths:
            commit_path = str(repository_path / dir_path.parts[-1] / path.relative_to(dir_path))
            
            if path.suffix in args.binary_ext:
                action = {
                    # Binary files need to be base64 encoded
                    'action': 'create',
                    'file_path': str(commit_path),
                    'content': base64.b64encode(open(path, mode='r+b').read()).decode(),
                    'encoding': 'base64',
                }
            else:
                action = {
                    # Text files
                    'action': 'create',
                    'file_path': str(commit_path),
                    'content': open(path).read(),
                }
            actions.append(action)
        # Create commit message
        extensions = ', '.join(path.suffix for path in paths)
        commit_message = f'{args.commit_message} {base_name} [{extensions}]'
        
        # Add the skipping of the bids-validation for each commit except the last
        if index != len(files_by_basename) - 1:
            commit_message += ' [skip-bids-validate]'
        
        data = {
            'branch': args.branch_name,
            'commit_message': commit_message,
            'actions': actions
        }
        
        # Create commit
        # python-gitlab uses timeout option on all requests, see
        # https://python-gitlab.readthedocs.io/en/stable/api-usage-advanced.html#timeout
        # Uploading large files on slow network might cause timeout errors.
        # We override the timeout value to account for that.
        commit = project.commits.create(data, timeout=3600)
        print(f'INFO Commit for {base_name} created successfully.')
        
        # Don't push the commit here
        
except Exception as e:
    print('[ERROR]', e)
    sys.exit(1)
