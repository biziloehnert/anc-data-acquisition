# ANC Data Acquisition

This project implements the MRI data acquisition workflow for the Austrian NeuroCloud project. The [workflow's documentation is under development](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/tree/main/acquisition/mri).

The workflow is implemented using [Nextflow](https://www.nextflow.io/).

## Usage

**Before using the pipeline, read the remainder of this README file.**

In order to start the data acquisition pipeline execute the following command, which first pulls the pipeline from the repository.
```bash
nextflow run https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition -revision git-ref -profile withapptainer --participantFile path/to/my/input/file
```
`path/to/my/input/file` is the path to the TAR file with subject DICOM files that should be converted (see [Input section](#input)).

`git-ref` is a version of the pipeline; use a git [tag](https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition/-/tags) for specific version or `main` for the newest changes on the `main` branch. When executing multiple versions of the pipeline, you may need to instruct Nextflow to pull the latest changes before execution, by adding the `-latest` flag.

You can also clone the project first and execute the local version of the pipeline from the root of the project with.
```bash
nextflow run main.nf -profile withapptainer --participantFile path/to/my/input/file
```

The `-C` option specifies the pipeline configuration file. If you copied the configuration to another file, change it in the command.
```bash
nextflow -C nextflow.config run main.nf -profile withapptainer --participantFile path/to/my/input/file
```

The workflow may generate significant amount of data. Use `-work-dir <path-to-work-dir>` option after the Nextflow script name in the command to specify where the workflow files will be stored.

## Basic concepts

The workflow converts data in DICOM format exported from MRI scanner into [BIDS format](https://bids-specification.readthedocs.io/en/stable/).

Currently, the workflow expects the DICOM files be in a TAR archive with a specific name. See [Input section](#input) for details. This is specific to Salzburg and is planned to be extended to other sites or more general usage. 

The BIDS conversion is performed using [BIDScoin](https://bidscoin.readthedocs.io/en/stable/) which further depends on [dcm2niix converter](https://github.com/rordenlab/dcm2niix).

The workflow is designed to be used on single subject data and push the converted data into a GitLab repository via a merge request. The GitLab part can be disabled and the workflow can be used for local conversion.

**IMPORTANT:** This pipeline assumes that **there is at least one session** in the data. This is in contrast to [optional session level in BIDS specification](https://bids-specification.readthedocs.io/en/stable/common-principles.html#filesystem-structure). Our argument is that this additional level does not hurt and makes the implementation less error-prone.

### GitLab specific assumptions

When subject data is pushed to GitLab, the following things happen:
- A new **branch** is created with name `sub-<subid>_ses-<sesid>`.
- A new **merge request** is created from branch `sub-<subid>_ses-<sesid>` to `main` branch.
- For each data file a new **file** is created in the GitLab project.
- The files are pushed with multiple commits, one for each file basename. This reduces the main memory consumption when the data is encoded for transfer (see #72+).

If any of these artifacts already exists, the acquisition pipeline **will fail** and the error may not be reported to the user. If the data acquisition procedure is followed, and [interrupted acquisitions](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/blob/main/format/datatypes/mri-conversion.md#interrupted-acquisitions) are correctly handled, the pipeline should not fail.

Guidelines, details, and correct BIDScoin mapping are [under development](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/issues/26).

Here are some rules:
- Do not export corrupted scans from the scanner.
- If the session was interrupted and the subject had to finish another day, add a suffix to the name of the second part of the session, e.g., `sub-12_ses-2part2`. You will need to rename files in the resulting merge request.

## Input

Currently, we consider only one input type to the pipeline. Though, it should not be difficult to implement more. Feel free to create an issue.

See the process `extract_participant` in the Nextflow file for the details.

The input is a TAR archive with DICOM files of a single session, organized in a [flat DICOM layout](https://bidscoin.readthedocs.io/en/stable/preparation.html#a-flat-dicom-layout).

The input TAR file name must implement the following pattern:
```
<unit group>_<target project>_<subject ID>_<session ID>_<date>_[<timestamp>].tar(.bz2|.xz)
```
where:
- `<unit group>` is the slug of the research unit group in the ANC repository where your dataset project is stored
- `<target project>` is the slug of your dataset project in the ANC repository, where the data should be injected
- `<subject ID>` is the BIDS participant label (without the `sub-` prefix)
- `<session ID>` is the BIDS session label (without the `ses-` prefix)
- `<date>` is the 8-digit date (irrelevant for the pipeline)
- `[<timestamp>]` is an optional 6-digit timestamp (irrelevant for the pipeline)

With group and project slugs we are more restrictive than the general [GitLab naming rules](https://docs.gitlab.com/ee/user/reserved_names.html#limitations-on-project-and-group-names). We allow only letters `(a-zA-Z)`, digits `(0-9)`, or dashes `(-)`.

We use the following regex to validate the file name:
```
/^([0-9A-Za-z-]+)_([0-9A-Za-z-]+)_([0-9A-Za-z]+)_([0-9A-Za-z]+)_([0-9]{8})(_[0-9]{6}){0,1}(\.tar)(\.bz2|\.xz)$/
```

The DICOM file names currently follow a hard-coded regex pattern, `.*\\/[A-Z]{2}\\d{6}`, which [will become configurable](https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition/-/issues/17). See the `sort_dicom` process in the Nextflow file. This is a legacy issue.

Example TAR file input and its contents:
```
A003_VP03_1_20230512_105617.tar.xz
└── A003_VP03_1_20230512_105617
    ├── MR000001
    ├── MR000002
    ├── MR000003
    ├── MR000004
    └── MR000005
```

## Workflow overview

```mermaid
flowchart TB
    subgraph " "
    v0["Channel.fromPath"]
    end
    v1([gitlab_mr_create])
    v3([extract_participant])
    v4([sort_dicom])
    v5([map_dicom])
    v6([convert_dicom])
    v7(["[Optional] discard_head_volumes"])
    v8(["[Optional] deface"])
    v9(["publishOutput OR gitlab_mr_files_commit"])
    v2(( ))
    v0 --> v3
    v1 --> v3
    v1 --> v4
    v1 --> v5
    v1 --> v6
    v1 --> v7
    v1 --> v8
    v1 --> v2
    v3 --> v4
    v4 --> v5
    v5 --> v6
    v6 --> v7
    v7 --> v8
    v8 --> v9
```

## Software dependencies

The workflow execution requires the following software (its further dependencies are not listed):
- Nextflow
- BIDScoin (BIDS conversion)
- dcm2niix (DICOM to NIFTI conversion)
- python-gitlab (as a module for using GitLab API in the `./bin/*` scripts)
- pydeface and FSL FLIRT (for optional defacing of structural images)
- Volumes Discard (for discarding obsolete fMRI volumes)

Installing such a software stack is not straightforward. Therefore, all the software is packaged in a Docker image. The Dockerfile and its assets are in [`docker` directory](./docker/). See [Docker image notes](#docker-image-notes) for more details.

### Local dependencies

The workflow is configured to be executed with Apptainer containers. Therefore, to execute it locally, install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) and [Apptainer](https://apptainer.org/docs/user/latest/quick_start.html#quick-installation). Using [Docker](https://docs.docker.com/desktop/linux/install/debian/) instead of Apptainer requires changing the configuration file.

#### python-gitlab

The workflow currently imports [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) module in the scripts used for *injecting data to ANC repository*. This module must be available. You can use Python virtual environment to install it:
```bash
python3 -m venv venv
source venv/bin/activate
pip install python-gitlab
```

#### Other local dependencies

`extract_participant` process is currently executed in the host. It uses `tar` tool and may require other compression libraries, for example `lbzip2`. The pipeline will error out with the missing dependencies.


### Docker image notes

FSL Flirt, the dependency of pydeface, and dcm2niix are [installed using Conda](https://open.win.ox.ac.uk/pages/fsl/docs/#/install/conda).

The image is then [shrinked using conda-pack](https://pythonspeed.com/articles/conda-docker-image-size/).

We fixed the dependencies version as follows:

| Software dependency | Version |
| :------------------ | :------ |
| fsl-flirt | 2111.2 |
| dcm2niix  | 1.0.20240202 |
| BIDScoin | 4.3.2 |
| Volumes Discard | 1.0.0 |
| pydeface | 2.0.2 |

### Centos7 notes

There is a separate Dockerfile for Centos7 and possibly other Linux systems with kernel older than 3.15. See the note in [`Docker_centos7` file](./docker/Dockerfile_centos7).

### Build the image

```bash
sudo docker build -t anc-data-acquisition-software -f docker/Dockerfile .
```

To build the CentOS 7 version:
```bash
sudo docker build -t anc-data-acquisition-centos7 -f docker/Dockerfile_centos7 .
```

### Execute shell in the container

```bash
sudo docker run --rm -it --entrypoint bash anc-data-acquisition-software
```

## Configuration

See the pipeline help message for available options.
```bash
nextflow run https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition -revision main --help
```

### Parameters summary

Parameters are passed in the form `--parameterName parameterValue`, for example, `--ancInject false`, after the Nextflow options. See [Usage section](#usage) and the help message for more details.

| Pipeline parameter | Description |
| :----------------- | :---------- |
| `--participantFile` | Path to [input file](#input). |
|`--volumesToDiscard`| The number of head volumes to be discarded from functional images. Useful mainly in Salzburg. Default: `0`. |
|`--deface`| Determine whether the defacing should be performed on the data. Default: `false`. |
|`--bidscoinMap`| Absolute path to BIDScoin map yaml file. The default map file is `${projectDir}/assets/bidscoin_map.yaml` and is delivered with this pipeline. |
| `--outputDir` | Absolute path to the directory where outputs of the pipeline (files in `sub-*/ses-*`) should be published. Used with `--ancInject false`. The default value is `${launchDir}/output`. |
| `--outputMode` | Mode of handling publishing the pipeline outputs, when `--ancInject false`. Allowed values: `"copy"` (the outputs will be copied to `outputDir`), `"link"` (symbolic links to the pipeline outputs will be created in `--outputDir`; useful for reviewing outputs and when the data is large). Default value is `"copy"`. |

`projectDir` and `launchDir` are Nextflow's variables set at runtime. They point to the pipeline root directory and the directory where the pipeline was executed, respectively.

| GitLab parameter | Description |
|:-----------------|:------------|
|`--ancInject` | Enable ANC data injection workflow. Default: `true`. |
|`--ancNamespace`| [Namespace](https://docs.gitlab.com/ee/user/namespace/) of the research unit group in the ANC repository, in which the target BIDS dataset project is stored. See [Input](#input) for information about project and groups. |
|`--pythonGitlabConfig`| Path to local [python-gitlab config file](#gitlab-authentication) which is required for GitLab authentication. This file is automatically bound inside the jobs container. Default: `~/.python-gitlab.cfg`. |

### Work directory

The pipeline stores intermediate results in a work directory, by default `<Nextflow launch directory>/work`. Use [`-work-dir` Nextflow option](https://www.nextflow.io/docs/latest/cli.html#run) to change it.

### Apptainer cache

The Apptainer images are cached by default in `${workDir}/singularity/` directory (yes, `singularity` is correct). To change it, set `cacheDir` variable in `profiles.withapptainer.apptainer` context in the pipeline config file.

### GitLab authentication

Pushing the data into a GitLab dataset project, and reporting the pipeline progress in a new merge request requires GitLab authentication. An access token with scope `api` must be provided. The pipeline uses [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/index.html) to communicate with the GitLab API.

By default the pipeline reads the authentication information from `~/.python-gitlab.cfg`. You can change the path to the configuration file with [`--pythonGitlabConfig` option](#parameters-summary). If it doesn't exist, create the configuration file, and provide `url` and `private_token` properties in  in section `[anc-data-acquisition]`. The following example configures authentication with the Austrian NeuroCloud:
```
[anc-data-acquisition]
url = https://gitlab.scc-pilot.plus.ac.at/
private_token = my-private-access-token
api_version = 4
```

If you use the pipeline personally, use [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). This ensures that your GitLab user is used for the contributions.

If you set up an automated pipeline execution, we recommend to use a personal access token of a [service account](https://docs.gitlab.com/ee/user/profile/service_accounts.html).

The configuration file is automatically bound inside of the jobs container to `/etc/python-gitlab.cfg`. This provides authentication for the GitLab actions from inside of the container.

## Testing

Testing assets are in `test` directory.

### Generate basic input data
Use [DICOM generator](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/bids-miscellaneous-scripts#generate-dicom-files)'s Docker image to generate inputs.

Basic input is specified in [`test/basic.json`](test).

Execute the following from the project root to generate DICOM files. If you prefer, clone the [DICOM generator](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/dicom-generator) project and execute the local version.

```
apptainer run --bind $PWD/test:/data/test docker://registry.gitlab.com/ccns/neurocog/neurodataops/anc/software/dicom-generator /data/test/basic.json /data/test/sub-001_ses-1 -t 'MR00000{id}'
TEST_FILE_NAME=group_project_001_1_$(date +%Y%m%d_%H%M%S).tar.xz
```

`group` and `project` in `TEST_FILE_NAME` are the group and project names of the dataset project. See [Input section](#input) for details.

Create a tar archive:

```
tar -cJf test/$TEST_FILE_NAME -C test/ sub-001_ses-1
```

### CI/CD

We automatically test the data acquisition workflow with GitLab's CI/CD mechanism. For details, see `.gitlab-ci.yml`. The workflow has a dedicated profile for CI/CD execution (`-profile cicd`). We generate dummy DICOM files, execute the pipeline including MR in a test project, clean up by deleting the branch.

Currently, we only test the version of the pipeline with GitLab workflow enabled.
