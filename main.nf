#!/usr/bin/env nextflow

// Declare syntax version
nextflow.enable.dsl=2

/*
 Pipeline parameters validation and parsing
 */

// Import nf-validation plugin
include { validateParameters; paramsHelp; paramsSummaryLog } from 'plugin/nf-validation'
// Print help message, supply typical command line usage for the pipeline
if (params.help) {
   log.info paramsHelp("nextflow main.nf --participantFile path/to/my/input/file [params]")
   exit 0
}
// Validate input parameters
validateParameters()
// Print summary of supplied parameters
log.info paramsSummaryLog(workflow)

/*
 Process participant file name and print the summary
 */

// Get participant info
// The pattern:
// <GitLab group>_<GitLab project>_<subject ID>_<session ID>_<date>_[<timestamp>].tar(.bz2|.xz)
participant_file_name_pattern = ~/^([0-9A-Za-z-]+)_([0-9A-Za-z-]+)_([0-9A-Za-z]+)_([0-9A-Za-z]+)_([0-9]{8})(_[0-9]{6}){0,1}(\.tar)(\.bz2|\.xz)$/

participant_file = file(params.participantFile)
participant_file_name = participant_file.getName()

// Error on invalid syntax
assert participant_file_name ==~ participant_file_name_pattern

participant_matcher = participant_file_name =~ participant_file_name_pattern

group = participant_matcher[0][1]
study = participant_matcher[0][2]
sub = participant_matcher[0][3]
ses = participant_matcher[0][4].replaceFirst(/_/, "")
// #echo "${BASH_REMATCH[5]}" # `DATE`
// #echo "${BASH_REMATCH[6]}" # `_TIME`
// #echo "${BASH_REMATCH[7]}" # `.tar`
compression = participant_matcher[0][8]

branch_name = "sub-" + sub
if (ses.length() > 0) {
    branch_name += "_ses-" + ses
}

project_id = params.ancNamespace + "/" + group + "/" + study

println("INFO GROUP:\t\t" + group)
println("INFO STUDY:\t\t" + study)
println("INFO SUBJECT:\t\t" + sub)
println("INFO SESSION:\t\t" + ses)
println("INFO COMPRESSION:\t" + compression)
println("INFO BRANCH_NAME:\t" + branch_name)
println("INFO PROJECT_ID:\t" + project_id)

//Extract subject's data in ${params.participantFile} tar file to ../work/*/dataset/sourcedata
process extract_participant {
    input:
        path dicom_tar
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            readonly TARGET_DIR="./dataset/sourcedata"

            readonly SUBJECT_DIR=\${TARGET_DIR}/sub-$sub/ses-$ses

            # [ ] Try to use `pbzip2` or `lbzip2` for faster decompression
            #     (https://askubuntu.com/questions/214458/any-linux-command-to-perform-parallel-decompression-of-tar-bz2-file)
            #     [ ] Verify if it's available on the host - install in image/add to local dependencies
            COMPRESSION_TOOL=""
            if [ $compression == ".bz2" ]; then
                COMPRESSION_TOOL="--use-compress-program=lbzip2"
            fi
            readonly COMPRESSION_TOOL

            echo "INFO Extracting $dicom_tar into \$SUBJECT_DIR"
            echo "INFO Compression used: $compression"
            echo "INFO Using '\${COMPRESSION_TOOL}' to extract archives"

            mkdir -p \$SUBJECT_DIR
            tar \$COMPRESSION_TOOL -xf $dicom_tar --directory \$SUBJECT_DIR --strip-components=1
            """
        if (params.ancInject == true)
            command +=
                """
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    'Subject DICOM files are extracted.'
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Sort DICOM files in ./dataset/ with BIDScoin
process sort_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition:1.1.1'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            dicomsort $dataset/sourcedata \
            --subprefix sub \
            --sesprefix ses \
            --pattern '.*\\/[A-Z]{2}\\d{6}'
            """
        if (params.ancInject == true)
            command +=
                """
                readonly DESCRIPTION=\$(ls -1 dataset/sourcedata/sub-$sub/ses-$ses)
                echo \$DESCRIPTION
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "DICOM files are sorted. Found the following sequences:\n<pre><code>\${DESCRIPTION}</code></pre>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Map DICOM files in ./dataset with BIDScoin
process map_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition:1.1.1'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            bidsmapper \
            --template ${params.bidscoinMap} -a \
            $dataset/sourcedata \
            $dataset
            """
        if (params.ancInject == true)
            command +=
                """
                readonly BIDSCOIN_LOG=\$(< $dataset/code/bidscoin/bidsmapper.log)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "BIDScoin mapping.\n\n<details><summary>Generated log</summary>\n<pre><code>\${BIDSCOIN_LOG}</code></pre>\n\n</details>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Converts DICOM files into BIDS format
process convert_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition:1.1.1'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            bidscoiner $dataset/sourcedata $dataset
            """
        if (params.ancInject == true)
            command +=
                """
                readonly BIDSCOIN_LOG=\$(< $dataset/code/bidscoin/bidscoiner.log)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "BIDScoin conversion.\n\n<details><summary>Generated log</summary>\n<pre><code>\${BIDSCOIN_LOG}</code></pre>\n\n</details>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Discard head volumns from fMRI NIFTI images
process discard_head_volumes {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition:1.1.1'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def note_body =
            """\
            Discarded \\`${params.volumesToDiscard}\\` volumes from functional images.
            
            This information is preserved in \\`*/func/*_bold.json\\` files under they key \\`NumberOfVolumesDiscardedByUser\\`.
            """.stripIndent()
        
        def command =
            """
            volumes-discard $dataset $dataset ${params.volumesToDiscard} --retain-paths
            """
        if (params.ancInject == true)
            command +=
                """
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "${note_body}"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

// Deface structural images `./dataset/sub-$sub/ses-$ses/**/*_T?w.nii.gz*`
process deface {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/anc-data-acquisition:1.1.1'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            # Find T? images in the session files.
            for f in \$(find $dataset/sub-$sub/ses-$ses/ -type f -name '*_T?w.nii*')
            do
                echo "INFO found \${f}"
                echo "INFO defacing \${f}"
                echo \${f#${dataset}/} >> defaced_files.txt
                pydeface \${f} --outfile \${f} --force --verbose
            done
            """
        if (params.ancInject == true)
            command +=
                """
                readonly DEFACED_FILES=\$(< defaced_files.txt)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "Defacing in the following files:\n<pre><code>\${DEFACED_FILES}</code></pre>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Adds all files from the subject durectory with content to the MR branch
process gitlab_mr_files_commit {
    input:
        path dataset

    """
    echo "INFO Committing $dataset/sub-$sub"
    gitlab_mr_files_commit.py \
        $project_id \
        $branch_name \
        $dataset/sub-$sub \
        '.' \
        "Adds " \
        -b .nii
    """
}

process gitlab_mr_create {
    output:
        stdout

    script:
        def mr_body =
            """\
            A new subject data has arrived for processing. See MR notes for any updates.

            |Subject property|Property value|
            |:--|:--|
            |File being processed|\\`$participant_file_name\\`|
            |Study abbreviation|\\`$study\\`|
            |Subject id|\\`$sub\\`|
            |Session id|\\`$ses\\`|
            """.stripIndent()

        def command =
            """
            echo "INFO GitLab disabled in process $task.process"
            """
        if (params.ancInject == true)
            command = 
                """
                gitlab_mr_create.py \
                    $project_id \
                    $branch_name \
                    --mr-title 'Add data of new subject $branch_name' \
                    --mr-description "${mr_body}"
                """
        return command
}

process publishOutput {
    input:
        path dataset
    
    script:
    if (params.outputMode == "link")
        """
        mkdir -p ${params.outputDir}/sub-${sub}
        echo "INFO Publishing pipeline outputs as symbolic links into ${params.outputDir}"
        ln -vsfr -t ${params.outputDir}/sub-${sub}/ ${dataset}/sub-${sub}/ses-${ses}
        """
    else if (params.outputMode == "copy")
        """
        mkdir -p ${params.outputDir}/sub-${sub}
        echo "INFO Copying pipeline outputs into ${params.outputDir}"
        cp -r ${dataset}/sub-${sub}/ses-${ses} ${params.outputDir}/sub-${sub}/
        """
    else
        error "Invalid value of params.outputMode: ${params.outputMode}"
}

workflow {
    participant_file_ch = channel.fromPath(params.participantFile)

    def mr_iid_val_ch = gitlab_mr_create()

    // A hack to be able to use MR iid in workflow.onComplete handler
    // Sets a script property with mr_iid value
    mr_iid_val_ch.subscribe { this.setProperty("mr_iid", it) }

    extract_participant(participant_file_ch, mr_iid_val_ch)
    sort_dicom(extract_participant.out, mr_iid_val_ch)
    map_dicom(sort_dicom.out, mr_iid_val_ch)
    convert_dicom(map_dicom.out, mr_iid_val_ch)

    dataset = convert_dicom.out
    if (params.volumesToDiscard > 0) {
        dataset = discard_head_volumes(dataset, mr_iid_val_ch)
    }
    if (params.deface == true) {
        dataset = deface(dataset, mr_iid_val_ch)
    }

    if (params.ancInject == true) {
        gitlab_mr_files_commit(dataset)
    } else {
        publishOutput(dataset)
    }
}

workflow.onComplete {
    // MR note with complete notification
    if (params.ancInject == true) {
        def note_body =
            """\
            Acquisition workflow completed.

            Execution status: **${ workflow.success ? 'SUCCESS' : 'FAIL' }**
            """.stripIndent()
        def cmd_list = [
            "${baseDir}/bin/gitlab_mr_note_create.py",
                    "${project_id}", 
                    this.getProperty("mr_iid"), 
                    "${note_body}"
            ]
        def proc = cmd_list.execute()
        proc.waitFor()
        // print(proc.err.text)
        // print(proc.in.text)
        // print(proc.exitValue())
    }
    println("INFO Acquisition workflow completed. Execution status: ${ workflow.success ? 'SUCCESS' : 'FAIL' }")
}

workflow.onError {
    // MR note with error notification
    if (params.ancInject == true) {
        def error_report = workflow.errorReport
        // This replace is necessary for the correct code formatting in MR note
        error_report = error_report.replaceAll("(?m)^", "            ")
        def note_body =
            """\
            An error occured during acquisition workflow. Please contact the administrator with the link to this note.
            
            <details>
            <summary>Workflow error report</summary>

            <pre><code>${error_report}</code></pre>

            </details>
            """.stripIndent()
        def cmd_list = [
            "${baseDir}/bin/gitlab_mr_note_create.py",
                    "${project_id}", 
                    this.getProperty("mr_iid"), 
                    "${note_body}"
            ]
        def proc = cmd_list.execute()
        proc.waitFor()
        // print(proc.err.text)
        // print(proc.in.text)
        // print(proc.exitValue())
    }
    println("ERROR An error occured during acquisition workflow. Execution status: ${ workflow.success ? 'SUCCESS' : 'FAIL' }")
}
